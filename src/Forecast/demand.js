import React, { Component } from "react"
import { Segment, Card } from 'semantic-ui-react'
import moment from 'moment'

class Forecast extends Component {

    static get dayOfWeekFactors() {
        return [3, 1, 2, 3, 4, 5, 3]
    }

    static get severityColors() {
        return ['green', 'yellow', 'orange', 'red']
    }

    endOfMonthFactor(date) {
        return moment(date).endOf('month').isSame(date, 'day') ? 5 : 0
    }

    dayOfWeekFactor(date) {
        let day = date.day()
        return this.constructor.dayOfWeekFactors[day]
    }

    weatherFactor(condition) {
        let text = condition.text
        if(/snow|sleet|blizzard|freezing drizzle|ice/i.test(text)) {
            return 5
        } else if(/rain|drizzle/i.test(text)) {
            return 4
        } else if(/sunny/i.test(text)) {
            return -2
        }
        return 0
    }

    color(rating) {
        // possible demand ratings are -1 to 15, so zero adjust to make it easier to determine severity
        let severity = Math.floor((rating + 1) / 4)
        return this.constructor.severityColors[severity]
    }

    rating() {
        let forecastDay = this.props.forecastDay
        let date = moment(forecastDay.date)
        let condition = forecastDay.day.condition
        return this.dayOfWeekFactor(date) + this.endOfMonthFactor(date) + this.weatherFactor(condition)
    }

    render() {
        let forecastDay = this.props.forecastDay
        let rating = this.rating()
        return (
            <Card key={forecastDay.date}>
                <Card.Content>
                    <Segment inverted color={this.color(rating)} textAlign='center'><h1>{rating}</h1></Segment>
                    <Card.Header>
                        {moment(forecastDay.date).format('dddd, MMMM Do YYYY')}
                    </Card.Header>
                </Card.Content>
            </Card>
        )
    }
}

export default Forecast