import React, { Component } from "react"
import { Segment, Loader, Dimmer } from 'semantic-ui-react'

class Loading extends Component {
    render() {
        return (
            <Segment basic style={{height: '100%'}}>
                <Dimmer active inverted>
                    <Loader inverted size='large' content='Loading' />
                </Dimmer>
            </Segment>
        )
    }
}

export default Loading