import React, { Component } from "react"
import { Menu } from 'semantic-ui-react'
import Kitchen from './kitchen'
import Loading  from './loading'
import ErrorMessage  from './error_message'
import './index.css'

class Forecast extends Component {

    componentWillMount() {
        this.fetchKitchens()
    }

    fetchKitchens() {
        this.setState({loading: true})
        fetch('https://api.staging.clustertruck.com/api/kitchens')
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.json()
            })
            .then(data => {
                let sortedKitchens = data.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
                let activeKitchen = sortedKitchens.length > 1 ? sortedKitchens[0] : null
                this.setState({kitchens: sortedKitchens, kitchen: activeKitchen, loading: false})
            })
            .catch(() => {
                this.setState({error: true, loading: false})
            })
    }

    renderKitchenMenu() {
        if(!this.state.kitchens) {
            return null
        }
        return (
            <Menu pointing vertical fixed='left'>
                <Menu.Item key="title">
                    <h3>ClusterTruck Demand Forecast</h3>
                </Menu.Item>
                {this.state.kitchens.map(kitchen => <Menu.Item key={kitchen.id} active={this.state.kitchen && kitchen.id === this.state.kitchen.id} onClick={this.setKitchen.bind(this, kitchen)}>{kitchen.name}</Menu.Item>)}
            </Menu>
        )
    }

    setKitchen(kitchen) {
        this.setState({kitchen: kitchen})
    }

    renderKitchen() {
        let kitchen = this.state.kitchen
        if(!kitchen) {
            return null
        }
        return <Kitchen key={kitchen.id} kitchen={kitchen} />
    }

    render() {
        if(this.state.loading) {
            return <Loading />
        }
        else if(this.state.error) {
            return <ErrorMessage />
        }
        return (
            <div>
                {this.renderKitchenMenu()}
                <div className="content">
                    {this.renderKitchen()}
                </div>
            </div>
        )
    }
}

export default Forecast