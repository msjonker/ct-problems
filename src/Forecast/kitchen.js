import React, { Component } from "react"
import { Card, Header, Segment } from 'semantic-ui-react'
import Demand from './demand'
import Loading  from './loading'
import ErrorMessage  from './error_message'

class Kitchen extends Component {

    fetchForecast(zipCode) {
        this.setState({loading: true})
        fetch(`http://api.apixu.com/v1/forecast.json?key=fd5b1e188f1342038b5232831170612&q=${zipCode}&days=10`)
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.json()
            })
            .then(data => this.setState({
                forecast: data,
                loading: false
            }))
            .catch(() => this.setState({
                error: true,
                loading: false
            }))
    }

    componentWillMount() {
        this.fetchForecast(this.props.kitchen.zip_code)
    }

    forecast() {
        let forecast = this.state.forecast
        if(!forecast) {
            return null
        }
        return forecast.forecast.forecastday.map(forecastDay => <Demand key={forecastDay.date} forecastDay={forecastDay} />)
    }

    render() {
        if(this.state.loading) {
            return <Loading />
        }
        else if(this.state.error) {
            return <ErrorMessage />
        }

        let kitchen = this.props.kitchen;
        return (
            <Segment basic key={kitchen.id}>
                <Header as='h2'>{kitchen.name}</Header>
                <Card.Group>
                    {this.forecast(kitchen)}
                </Card.Group>
            </Segment>
        )
    }
}

export default Kitchen