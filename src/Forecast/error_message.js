import React, { Component } from "react"
import { Message } from 'semantic-ui-react'

class ErrorMessage extends Component {
    render() {
        return (
            <Message negative>
                <Message.Header>There was an error processing your request.</Message.Header>
                <p>Please try again.</p>
            </Message>
        )
    }
}

export default ErrorMessage